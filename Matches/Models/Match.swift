import Foundation

struct Match: Decodable{
    var date: String?
    let tournament: Tournament?
    let team1: Team?
    let team2: Team?
    let calc: Bool?
    let has_video: Bool?
    let live: Bool?
    let storage: Bool?
    let sub: Bool?
}
struct Tournament: Decodable {
    let id: Int?
    let name_eng: String?
    let name_rus: String?
}
struct Team: Decodable {
    let id: Int?
    let name_eng: String?
    let name_rus: String?
    let abbrev_eng: String?
    let abbrev_rus: String?
    let score: Int?
}

