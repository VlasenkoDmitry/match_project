import Foundation

class Networking {
    let error404 = "404: Not Found"
    static let shared = Networking()
    private init() {}
    var urlData: String {
        get {
            return JSONDataForRequest.shared.urlData
        }
    }
    var urlVideo : String {
        get {
            return JSONDataForRequest.shared.urlVideo
        }
    }
    func sendPostRequest<T:Decodable>(body: [String: Any],model: T.Type,data: DataLoading, then completion: @escaping (T?, Error?) -> Void) {
        var urlString = String()
        switch data {
        case .video:
            urlString = urlVideo
        case .data:
            urlString = urlData
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            guard let url = NSURL(string: urlString) else { return }
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                guard let data = data else { return }
                var result = Parse.shared.decodeJSON(data: data, model: model.self)
                if result == nil {
                    let str = String(decoding: data, as: UTF8.self)
                    if str == self.error404 {
                        print("Error -> \(self.error404)")
                    }
                    if error != nil{
                        print("Error -> \(error)")
                    }
                }
                completion(result, error)
            }
            task.resume()
        } catch {
            print(error)
        }
    }
}

