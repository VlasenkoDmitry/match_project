import Foundation

class Parse{
    static let shared = Parse()
    
    func decodeJSON<T: Decodable>(data: Data, model: T.Type) -> T?{
        let decoder = JSONDecoder()
        do {
            let result = try decoder.decode(model.self, from: data)
            return result
        } catch {
            print(error)
        }
        return nil
    }
}
