import Foundation
import UIKit

class MatchDisplayedData {
    var tournament: String?
    var nameTeamFirst: String?
    var nameTeamSecond: String?
    var dateMatch: String?
    var score: String?
    
    init(tournament: String?,nameTeamFirst: String? , nameTeamSecond: String?, dateMatch: String?, score: String? ) {
        self.tournament = tournament
        self.nameTeamFirst = nameTeamFirst
        self.nameTeamSecond = nameTeamSecond
        self.dateMatch = dateMatch
        self.score = score
    }
}
