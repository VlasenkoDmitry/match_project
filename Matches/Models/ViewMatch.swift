import Foundation
import UIKit

class ViewMatch {
    var view = UIView()
    
    init() {
        view = self.initialization()
    }
    
    func initialization() -> UIView {
        let viewMatch = UIView()
        viewMatch.frame = CGRect(x: 16, y: 44, width: 400, height: 250)
        viewMatch.corn()
        
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: viewMatch.frame.size.width, height: viewMatch.frame.size.height)
        image.image = UIImage(named: "ronaldo-vs-Messi")
        image.contentMode = .scaleAspectFill
        image.layer.masksToBounds = true
        image.layer.cornerRadius = 20
        viewMatch.addSubview(image)
        
        let overImageBackgroungView = UIView()
        overImageBackgroungView.frame = CGRect(x: 0, y: 0, width: viewMatch.frame.size.width, height: viewMatch.frame.size.height)
        overImageBackgroungView.backgroundColor = .black
        overImageBackgroungView.alpha = 0.6
        overImageBackgroungView.corn()
        viewMatch.addSubview(overImageBackgroungView)
        
        let viewTournament = UIView()
        let viewDate = UIView()
        let viewNameTeams = UIView()
        let viewButtons = UIView()
        let arrayViews = [viewTournament,viewDate,viewNameTeams,viewButtons]
        for i in arrayViews {
            viewMatch.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
            i.frame.size.width = viewMatch.frame.self.width
            i.backgroundColor = .clear
        }
        
        let constraints = [
            viewTournament.heightAnchor.constraint(equalToConstant: 50),
            viewTournament.topAnchor.constraint(equalTo: viewMatch.topAnchor, constant: 0),
            viewTournament.leftAnchor.constraint(equalTo: viewMatch.leftAnchor, constant: 0),
            viewTournament.rightAnchor.constraint(equalTo: viewMatch.rightAnchor, constant: 0),
            viewDate.heightAnchor.constraint(equalToConstant: 50),
            viewDate.topAnchor.constraint(equalTo: viewTournament.bottomAnchor, constant: 0),
            viewDate.leftAnchor.constraint(equalTo: viewMatch.leftAnchor, constant: 0),
            viewDate.rightAnchor.constraint(equalTo: viewMatch.rightAnchor, constant: 0),
            viewNameTeams.heightAnchor.constraint(equalToConstant: 50),
            viewNameTeams.topAnchor.constraint(equalTo: viewDate.bottomAnchor, constant: 0),
            viewNameTeams.leftAnchor.constraint(equalTo: viewTournament.leftAnchor, constant: 0),
            viewNameTeams.rightAnchor.constraint(equalTo: viewTournament.rightAnchor, constant: 0),
            viewButtons.heightAnchor.constraint(equalToConstant: 100),
            viewButtons.topAnchor.constraint(equalTo: viewNameTeams.bottomAnchor, constant: 0),
            viewButtons.leftAnchor.constraint(equalTo: viewMatch.leftAnchor, constant: 0),
            viewButtons.bottomAnchor.constraint(equalTo: viewMatch.bottomAnchor, constant: 0),
            viewButtons.rightAnchor.constraint(equalTo: viewMatch.rightAnchor, constant: 0),
        ]
        NSLayoutConstraint.activate(constraints)
        
        let tournamentLabel = UILabel()
        let nameTeamFirstLabel = UILabel()
        let nameTeamSecondLabel = UILabel()
        let dateMatchLabel = UILabel()
        let scoreLable = UILabel()
        let arrayLabel = [tournamentLabel,nameTeamFirstLabel,nameTeamSecondLabel,dateMatchLabel,scoreLable]
        for i in arrayLabel {
            i.formatLabel()
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        viewTournament.addSubview(tournamentLabel)
        viewDate.addSubview(dateMatchLabel)
        viewNameTeams.addSubview(nameTeamFirstLabel)
        viewNameTeams.addSubview(nameTeamSecondLabel)
        viewNameTeams.addSubview(scoreLable)
        
        let firstHalfButton = UIButton()
        let secondHalfButton = UIButton()
        firstHalfButton.formatButton()
        firstHalfButton.backgroundColor = .red
        secondHalfButton.formatButton()
        secondHalfButton.backgroundColor = .blue
        firstHalfButton.titleLabel?.numberOfLines = 0
        secondHalfButton.titleLabel?.numberOfLines = 0
        viewButtons.addSubview(firstHalfButton)
        viewButtons.addSubview(secondHalfButton)
        
        let constraintsLabel = [
            tournamentLabel.centerXAnchor.constraint(equalTo: viewTournament.centerXAnchor),
            tournamentLabel.centerYAnchor.constraint(equalTo: viewTournament.centerYAnchor),
            dateMatchLabel.centerXAnchor.constraint(equalTo: viewDate.centerXAnchor),
            dateMatchLabel.centerYAnchor.constraint(equalTo: viewDate.centerYAnchor),
            scoreLable.centerYAnchor.constraint(equalTo: viewNameTeams.centerYAnchor),
            scoreLable.centerXAnchor.constraint(equalTo: viewNameTeams.centerXAnchor),
            nameTeamFirstLabel.centerYAnchor.constraint(equalTo: viewNameTeams.centerYAnchor),
            nameTeamFirstLabel.rightAnchor.constraint(equalTo: scoreLable.leftAnchor, constant: 0),
            nameTeamFirstLabel.leftAnchor.constraint(equalTo: viewTournament.leftAnchor, constant: 0),
            nameTeamSecondLabel.centerYAnchor.constraint(equalTo: viewNameTeams.centerYAnchor),
            nameTeamSecondLabel.leftAnchor.constraint(equalTo: scoreLable.rightAnchor, constant: 10),
            nameTeamSecondLabel.rightAnchor.constraint(equalTo: viewTournament.rightAnchor, constant: 0),
            firstHalfButton.centerYAnchor.constraint(equalTo: viewButtons.centerYAnchor),
            firstHalfButton.leftAnchor.constraint(equalTo: viewButtons.leftAnchor, constant: 10),
            secondHalfButton.centerYAnchor.constraint(equalTo: viewButtons.centerYAnchor),
            secondHalfButton.rightAnchor.constraint(equalTo: viewButtons.rightAnchor, constant: -10),
            firstHalfButton.heightAnchor.constraint(equalTo: firstHalfButton.titleLabel!.heightAnchor, constant: 0),
            firstHalfButton.widthAnchor.constraint(equalToConstant: 100),
            secondHalfButton.heightAnchor.constraint(equalTo: secondHalfButton.titleLabel!.heightAnchor, constant: 0),
            secondHalfButton.widthAnchor.constraint(equalToConstant: 100)
        ]
        NSLayoutConstraint.activate(constraintsLabel)
        
        tournamentLabel.text = "Tournament"
        nameTeamFirstLabel.text = "First"
        nameTeamSecondLabel.text = "Second"
        dateMatchLabel.text = "Date"
        scoreLable.text = "1 - 2"
        firstHalfButton.setTitle("Video first half", for: .normal)
        firstHalfButton.titleLabel?.textAlignment = .center
        secondHalfButton.setTitle("Video second half", for: .normal)
        secondHalfButton.titleLabel?.textAlignment = .center
        tournamentLabel.font = UIFont.boldSystemFont(ofSize: 20)
        dateMatchLabel.font = UIFont.systemFont(ofSize: 17)
        scoreLable.font = UIFont.boldSystemFont(ofSize: 35)
        nameTeamFirstLabel.font = UIFont.boldSystemFont(ofSize: 25)
        nameTeamSecondLabel.font = UIFont.boldSystemFont(ofSize: 25)
        firstHalfButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        secondHalfButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        
        tournamentLabel.tag = 1
        nameTeamFirstLabel.tag = 2
        nameTeamSecondLabel.tag = 3
        dateMatchLabel.tag = 4
        scoreLable.tag = 5
        firstHalfButton.tag = 1
        secondHalfButton.tag = 2
        
        let picker = UIPickerView()
        viewButtons.addSubview(picker)
        picker.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            picker.heightAnchor.constraint(equalToConstant: 100),
            picker.widthAnchor.constraint(equalToConstant: 140),
            picker.centerYAnchor.constraint(equalTo: viewButtons.centerYAnchor),
            picker.centerXAnchor.constraint(equalTo: viewButtons.centerXAnchor),
        ])
        
        return viewMatch
    }
}
