//
//  JSONDataForRequest.swift
//  Matches
//
//  Created by Ap on 16.01.22.
//

import Foundation

class JSONDataForRequest {
    static let shared = JSONDataForRequest()
    
    var jsonVideo: [String: Any] = ["match_id": 1724836,"sport_id": 1]
    var jsonData: [String: Any] = ["proc": "get_match_info",
                               "params": ["_p_sport":1,"_p_match_id": 1724836]]
    var urlData = "https://api.instat.tv/test/data"
    var urlVideo = "https://api.instat.tv/test/video-urls"
}


