import Foundation
import UIKit

extension UILabel {
    func formatLabel() {
        self.frame.size.width = CGFloat(150)
        self.frame.size.height = CGFloat(20)
        self.textAlignment = .center
        self.textColor = .white
    }
}
