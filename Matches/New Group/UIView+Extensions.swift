import Foundation
import UIKit

extension UIView {
    func corn(int: CGFloat = 20) {
        self.layer.cornerRadius = int
    }
}

