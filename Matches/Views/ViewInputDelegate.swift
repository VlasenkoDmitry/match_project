import Foundation
import UIKit

protocol ViewInputDelegate: AnyObject {
    func setupInitialize()
    func setupData(matchData: Match)
    func playVideo(urlString: String)
    func addView(view: UIView)
}
