import Foundation

protocol ViewOutputDelegate: AnyObject {
    func getData()
    func getVideo(half: Int, definition: Int)
}
