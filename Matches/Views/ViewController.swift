import UIKit
import AVKit

class ViewController: UIViewController {
    private let presentor = Presenter()
    private var matchData = [Match?]()
    private var viewMatch = UIView()
    private weak var viewOutputDelegate: ViewOutputDelegate?
    var player: AVAudioPlayer?
    var definitionVideo = [String]()
    var numberLineDefinitionVideo = 0 {
        didSet {
            presentor.numberLineDefinitionVideo = self.numberLineDefinitionVideo
        }
    }
    var arrayView = [UIView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definitionVideo = ["240p","400p","720p","1080p"]
        presentor.setViewInputDelegate(viewInputDelegate: self)
        self.viewOutputDelegate = presentor
        presentor.addNewViewInArray(controller: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.addSubview(arrayView[0])
        self.viewOutputDelegate?.getData()
    }
    
}

extension ViewController: ViewInputDelegate {
    func addView(view: UIView) {
        arrayView.append(view)
    }
    
    func setupData(matchData: Match) {
        self.matchData.append(matchData)
    }
    
    func playVideo(urlString: String) {
        guard let videoURL = URL(string: urlString) else {return}
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player?.play()
        }
    }
    
    func setupInitialize() {
        var arrayLabels = [UILabel]()
        if let match = self.matchData[0] {
            let allView: [UIView] = presentor.getSubviewsOfView(view: viewMatch)
            for view in allView {
                if let label = view as? UILabel {
                    arrayLabels.append(label)
                }
                for i in arrayLabels {
                    switch i.tag {
                    case 1:
                        i.text = match.tournament?.name_eng
                    case 2:
                        i.text = match.team1?.name_eng
                    case 3:
                        i.text = match.team2?.name_eng
                    case 4:
                        guard let data = match.date  else {continue}
                        i.text = String(data.dropLast(4))
                    case 5:
                        guard let score1 = match.team1?.score  else {continue}
                        guard let score2 = match.team2?.score  else {continue}
                        i.text = String(score1) + " - " + String(score2)
                    default:
                        continue
                    }
                }
            }
        }
    }

}


extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return definitionVideo[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        numberLineDefinitionVideo = pickerView.selectedRow(inComponent: component)
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = definitionVideo[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: UIColor.yellow])
        return myTitle
    }
}
