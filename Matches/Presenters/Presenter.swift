import Foundation
import UIKit

class Presenter {
    weak private var viewInputDelegate: ViewInputDelegate?
    var matchData: [Match]?
    var videoData: [Video]?
    var numberLineDefinitionVideo = 0
    var jsonVideo: [String:Any] {
        get {
            return JSONDataForRequest.shared.jsonVideo
        }
    }
    var jsonData: [String: Any] {
        get {
            return JSONDataForRequest.shared.jsonData
        }
    }
    
    func addNewViewInArray(controller: ViewController) {
        let viewMatch = ViewMatch.init().view
        viewMatch.frame = CGRect(x: 16, y: 44, width: Int(controller.view.frame.size.width) - 32, height: 250)
        let allSubView = getSubviewsOfView(view: viewMatch)
        for view in allSubView {
            if let btn = view as? UIButton {
                btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            }
            if let picker = view as? UIPickerView {
                picker.dataSource = controller
                picker.delegate = controller
            }
        }
        self.viewInputDelegate?.addView(view: viewMatch)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        self.getVideo(half: sender.tag, definition: numberLineDefinitionVideo)
    }
    
    func getSubviewsOfView(view: UIView) -> [UIView] {
        var subviewArray = [UIView]()
        if view.subviews.count == 0 {
            return subviewArray
        }
        for subview in view.subviews {
            subviewArray += self.getSubviewsOfView(view: subview)
            subviewArray.append(subview)
        }
        return subviewArray
    }
    
    func setViewInputDelegate(viewInputDelegate:ViewInputDelegate?) {
        self.viewInputDelegate = viewInputDelegate
    }
    
    func loadDataMatch<T:Decodable>(json: [String: Any], data: DataLoading, model: T.Type, completion: @escaping ((Bool)->())) {
        Networking.shared.sendPostRequest(body: json, model: model, data: data) { result, error in
            var boolResult = false
            if result != nil {
                if model == [Video].self {
                    self.videoData = result as? [Video]
                    boolResult = true
                }
                if model == Match.self {
                    guard let result = result as? Match else { return }
                    self.matchData?.append(result)
                    self.viewInputDelegate?.setupData(matchData: result)
                    boolResult = true
                }
            }
            completion(boolResult)
        }
    }
}

extension Presenter: ViewOutputDelegate {
    func getVideo(half: Int, definition: Int) {
        loadDataMatch(json: jsonVideo, data: .video, model: [Video].self) {result in
            if result == true {
                DispatchQueue.main.async {
                    var url = String()
                    if half == 1 {
                        guard let videoURL = self.videoData?[definition].url else {return}
                        url = videoURL
                    } else {
                        guard let videoURL = self.videoData?[definition * 2].url else {return}
                        url = videoURL
                    }
                    self.viewInputDelegate?.playVideo(urlString: url)
                }
            } else {
                print("video is not found")
            }
        }
    }
    
    func getData() {
        loadDataMatch(json: jsonData, data: .data, model: Match.self) { result in
            if result == true {
                DispatchQueue.main.async {
                    self.viewInputDelegate?.setupInitialize()
                }
            } else {
                print("data is not found")
            }
        }
    }
}


